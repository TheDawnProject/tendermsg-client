# tendermsg client

A QT-based multi-platform native messaging system that can 


* preserve your chats publicly forever 
* go "off the record" if needed 
* or keep an encrypted record of your chats only accessible to you by storing it in an encrypted state on the blockchain.  

